package org.kds.spring.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;


import org.kds.spring.jpa.domain.Cliente;

public interface ClientsJPARepository extends JpaRepository<Cliente, Long>{

}
