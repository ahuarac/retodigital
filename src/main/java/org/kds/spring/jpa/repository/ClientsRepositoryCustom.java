package org.kds.spring.jpa.repository;

import org.kds.spring.jpa.domain.Cliente ;
import org.kds.spring.jpa.domain.Employees;
import org.kds.spring.jpa.domain.KpiClients;

import java.util.List;


public interface ClientsRepositoryCustom {
	
	   List<Cliente> getAllClients();
	   List<KpiClients> getAllKpiClients();
}
