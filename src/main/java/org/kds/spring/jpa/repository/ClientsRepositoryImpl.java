package org.kds.spring.jpa.repository;

import org.kds.spring.jpa.domain.Cliente;
import org.kds.spring.jpa.domain.KpiClients;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.List;

public class ClientsRepositoryImpl implements ClientsRepositoryCustom {

	
	  @PersistenceContext
	    private EntityManager em;


	    @Override
	    public List<Cliente> getAllClients() {
	        StoredProcedureQuery findByYearProcedure =
	                em.createNamedStoredProcedureQuery("getAllClients");
	        return findByYearProcedure.getResultList();
	    }
	    
	    @Override
	    public List<KpiClients> getAllKpiClients() {
	        StoredProcedureQuery findByYearProcedure =
	                em.createNamedStoredProcedureQuery("getKpiClients");
	        return findByYearProcedure.getResultList();
	    }
}
