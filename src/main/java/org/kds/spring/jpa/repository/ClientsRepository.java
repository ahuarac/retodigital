package org.kds.spring.jpa.repository;

import org.kds.spring.jpa.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientsRepository  extends CrudRepository<Cliente, Long>, ClientsRepositoryCustom{

}
