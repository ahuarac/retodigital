package org.kds.spring.jpa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kds.spring.jpa.dao.ClientsJPARepository;
import org.kds.spring.jpa.domain.Cliente;

@Service
public class Clientsservice {

	   @Autowired
	    ClientsJPARepository dao;
	     
	    // (3)
	    public Cliente save(Cliente clients){
	        return dao.saveAndFlush(clients);
	    }
}
