package org.kds.spring.jpa.controller;

import org.kds.spring.jpa.domain.Employees;
import org.kds.spring.jpa.domain.Cliente;
import org.kds.spring.jpa.domain.KpiClients;
import org.kds.spring.jpa.domain.Promedios;
import org.kds.spring.jpa.domain.ListClients;
import org.kds.spring.jpa.service.Clientsservice;
import org.kds.spring.jpa.repository.EmployeeRepository;
import org.kds.spring.jpa.repository.ClientsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private  Clientsservice clientsService;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Employees>> getAllEmployees() {

        Iterable<Employees> employees = employeeRepository.getAllEmployees();

        List<Employees> target = new ArrayList<>();
        employees.forEach(target::add);
        return new ResponseEntity<>(target, HttpStatus.OK);

    }
    @RequestMapping(value="/clientes", method=RequestMethod.POST)
    public Cliente updateOrSave(@RequestBody Cliente cliente){
       
        return clientsService.save(cliente);
    }
    
    @RequestMapping(value = "/listclientes", method = RequestMethod.GET)
    public ResponseEntity<List<ListClients>> getAllClients() {

        Iterable<Cliente> clients = clientsRepository.getAllClients();

        List<ListClients> target = new ArrayList<>();
        ListClients cli ;
        int anio,mes,dia=0;
        for (Cliente p:clients) { 
			cli= new ListClients();
			cli.setClientID(p.getClientID());
			cli.setNombre(p.getNombre());
			cli.setApellido(p.getApellido());
			cli.setEdad(p.getEdad());
			cli.setFechaNacimiento(p.getFechaNacimiento());
			anio= Integer.parseInt(cli.getFechaNacimiento().split("-")[0]) +50;
			mes=Integer.parseInt(cli.getFechaNacimiento().split("-")[1]);
			dia=Integer.parseInt(cli.getFechaNacimiento().split("-")[2]);
			cli.setFechaMuerte(anio+"-"+mes+"-"+dia); 
				 
						target.add(cli);
        		}
    //    clients.forEach(target::add);
        return new ResponseEntity<>(target, HttpStatus.OK);

    }
    
    @RequestMapping(value = "/kpideclientes", method = RequestMethod.GET)
    public ResponseEntity<List<Promedios>>  getAllKpiClients() {

        Iterable<KpiClients> clients = clientsRepository.getAllKpiClients();

        List<Promedios> target = new ArrayList<>();
       // clients.forEach(target::add);
        		Promedios o ;
        for (KpiClients p:clients) { 
        				o= new Promedios();
        	 	o.setPromEdad(p.getPromEdad());
         		o.setDesviacionEstandar(p.getDesviacionEstandar());
         		target.add(o);
        	}
       return new ResponseEntity<>(target, HttpStatus.OK);
      
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Employees> getAllEmployeeById(@PathVariable(name = "id") long id) {

        Employees one = employeeRepository.findOne(id);
        return new ResponseEntity<Employees>(one, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/listclientes/{id}", method = RequestMethod.GET)
    public ResponseEntity<Cliente> getAllClientsById(@PathVariable(name = "id") long id) {

    	Cliente one = clientsRepository.findOne(id);
        return new ResponseEntity<Cliente>(one, HttpStatus.OK);
    }
}
