package org.kds.spring.jpa.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class ListClients {

	
	    @Id
		@GeneratedValue
		    private long clientID;
		  //  @Column(name = "nombre")
		    private String nombre;
		  //  @Column(name = "apellido")
		    private String apellido;
		  //  @Column(name = "edad")
		    private String edad;
		   // @Column(name = "fechaNacimiento")
		    private String fechaNacimiento;
		    
		    private String fechaMuerte;

			public long getClientID() {
				return clientID;
			}

			public void setClientID(long clientID) {
				this.clientID = clientID;
			}

			public String getNombre() {
				return nombre;
			}

			public void setNombre(String nombre) {
				this.nombre = nombre;
			}

			public String getApellido() {
				return apellido;
			}

			public void setApellido(String apellido) {
				this.apellido = apellido;
			}

			public String getEdad() {
				return edad;
			}

			public void setEdad(String edad) {
				this.edad = edad;
			}

			public String getFechaNacimiento() {
				return fechaNacimiento;
			}

			public void setFechaNacimiento(String fechaNacimiento) {
				this.fechaNacimiento = fechaNacimiento;
			}

			public String getFechaMuerte() {
				return fechaMuerte;
			}

			public void setFechaMuerte(String fechaMuerte) {
				this.fechaMuerte = fechaMuerte;
			}
}
