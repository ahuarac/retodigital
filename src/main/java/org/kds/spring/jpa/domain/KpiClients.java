package org.kds.spring.jpa.domain;
 
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "client")
 

@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery(name = "getKpiClients",
                                procedureName = "get_all_kpiClients",
    resultClasses = KpiClients.class)
})
public class KpiClients {

	
	@Id
    @Column(name = "clientID")
    private long clientID;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "edad")
    private String edad;
    @Column(name = "fechaNacimiento")
    private String fechaNacimiento;
    @Column(name = "promEdad")
    private String promEdad;
    @Column(name = "desviacionEstandar")
    private String desviacionEstandar;
    
	public long getClientID() {
		return clientID;
	}
	public void setClientID(long clientID) {
		this.clientID = clientID;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getPromEdad() {
		return promEdad;
	}
	public void setPromEdad(String promEdad) {
		this.promEdad = promEdad;
	}
	public String getDesviacionEstandar() {
		return desviacionEstandar;
	}
	public void setDesviacionEstandar(String desviacionEstandar) {
		this.desviacionEstandar = desviacionEstandar;
	}
}
