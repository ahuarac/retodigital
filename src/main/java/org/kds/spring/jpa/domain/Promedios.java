package org.kds.spring.jpa.domain;
 

import javax.persistence.*;
import java.io.Serializable;

public class Promedios {

	
	   @Column(name = "promEdad")
	    private String promEdad;
	    @Column(name = "desviacionEstandar")
	    private String desviacionEstandar;
	    
		public String getPromEdad() {
			return promEdad;
		}
		public void setPromEdad(String promEdad) {
			this.promEdad = promEdad;
		}
		public String getDesviacionEstandar() {
			return desviacionEstandar;
		}
		public void setDesviacionEstandar(String desviacionEstandar) {
			this.desviacionEstandar = desviacionEstandar;
		}
}
