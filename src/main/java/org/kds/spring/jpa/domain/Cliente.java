package org.kds.spring.jpa.domain;

 

//import javax.persistence.*;
import java.io.Serializable; 

import javax.persistence.Table;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Table(name = "client") 

@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery(name = "getAllClients",
                                procedureName = "get_all_Clientes",
    resultClasses = Cliente.class)
}) 
public class Cliente implements Serializable {

	  // (3) 
  private static final long serialVersionUID = 4894729030347835498L;

		
	 //   @Column(name = "clientID")
  @Id
	@GeneratedValue
	    private long clientID;
	  //  @Column(name = "nombre")
	    private String nombre;
	  //  @Column(name = "apellido")
	    private String apellido;
	  //  @Column(name = "edad")
	    private String edad;
	   // @Column(name = "fechaNacimiento")
	    private String fechaNacimiento;
	    
	    public Cliente(){
			
		}
	    
	    
	    public Cliente(Long clientID, String nombre, String apellido, String edad, String fechaNacimiento) {
			super();
			this.clientID = clientID;
			this.nombre = nombre;
			this.apellido = apellido;
			this.edad = edad;
			this.fechaNacimiento = fechaNacimiento;
		}
	    
		public long getClientID() {
			return clientID;
		}
		public void setClientID(long clientID) {
			this.clientID = clientID;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getApellido() {
			return apellido;
		}
		public void setApellido(String apellido) {
			this.apellido = apellido;
		}
		public String getEdad() {
			return edad;
		}
		public void setEdad(String edad) {
			this.edad = edad;
		}
		public String getFechaNacimiento() {
			return fechaNacimiento;
		}
		public void setFechaNacimiento(String fechaNacimiento) {
			this.fechaNacimiento = fechaNacimiento;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
	   
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Client [clientID=");
			builder.append(clientID);
			builder.append(", nombre=");
			builder.append(nombre);
			builder.append(", apellido=");
			builder.append(apellido);
			builder.append(", edad=");
			builder.append(edad);
			builder.append(", fechaNacimiento=");
			builder.append(fechaNacimiento);
			builder.append("]");
			return builder.toString();
		}
}
